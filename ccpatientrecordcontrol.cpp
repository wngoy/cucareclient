#include "ccpatientrecordcontrol.h"

CCPatientRecordControl::CCPatientRecordControl()
{
}

CCPatientRecordControl::~CCPatientRecordControl()
{

}

CCPatientRecord* CCPatientRecordControl::getPatientRecordForId(int aPatientRecordId)
{
    if (_patientRecordsMap.size() < 0 || (!_patientRecordsMap.contains(aPatientRecordId)))
    {
        this->refreshPatientRecords();
    }

    if (_patientRecordsMap.contains(aPatientRecordId))
    {
        return _patientRecordsMap[aPatientRecordId];
    }

    return 0;
}

void CCPatientRecordControl::refreshPatientRecords()
{
    _patientRecordsMap.clear();

    // call server and refresh map
}

QMap<int, CCPatientRecord*> CCPatientRecordControl::getPatientRecordsMap(){
    return _patientRecordsMap;
}
