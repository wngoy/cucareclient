#ifndef CCPHYSICIANRECORD_H
#define CCPHYSICIANRECORD_H

#include <QString>

class CCPhysicianRecord
{
public:
    CCPhysicianRecord(int aId, QString aFirstName, QString aLastName);
    CCPhysicianRecord(QString constructorString);
    ~CCPhysicianRecord();

    QString firstName;
    QString lastName;
    int physicianId;
};

#endif // CCPHYSICIANRECORD_H
