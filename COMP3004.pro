#-------------------------------------------------
#
# Project created by QtCreator 2012-10-24T18:21:28
#
#-------------------------------------------------

QT       += core gui

TARGET = COMP3004
TEMPLATE = app


SOURCES += main.cpp\
        homewindow.cpp \
    loginwindow.cpp \
    systemhomewindow.cpp \
    manageconsultationswindow.cpp \
    ccuserdatacontrol.cpp \
    ccconsultationrecord.cpp \
    ccpatientrecord.cpp \
    ccphysicianrecord.cpp \
    ccclinicrecord.cpp \
    ccconsultationtype.cpp \
    editconsultationswindow.cpp \
    ccconsultationtypecontrol.cpp \
    ccutils.cpp \
    ccpatientrecordcontrol.cpp \
    ccclinicrecordcontrol.cpp \
    ccphysicianrecordcontrol.cpp \
    ccconsultationrecordcontrol.cpp \
    addconsultationwindow.cpp \
    patientselectionwindow.cpp \
    physicianselectionwindow.cpp \
    viewconsultationwindow.cpp

HEADERS  += homewindow.h \
    loginwindow.h \
    systemhomewindow.h \
    manageconsultationswindow.h \
    ccuserdatacontrol.h \
    ccconsultationrecord.h \
    ccpatientrecord.h \
    ccphysicianrecord.h \
    ccclinicrecord.h \
    ccconsultationtype.h \
    editconsultationswindow.h \
    ccconsultationtypecontrol.h \
    ccutils.h \
    ccpatientrecordcontrol.h \
    ccclinicrecordcontrol.h \
    ccphysicianrecordcontrol.h \
    ccconsultationrecordcontrol.h \
    addconsultationwindow.h \
    patientselectionwindow.h \
    physicianselectionwindow.h \
    viewconsultationwindow.h

FORMS    += homewindow.ui \
    loginwindow.ui \
    systemhomewindow.ui \
    manageconsultationswindow.ui \
    editconsultationswindow.ui \
    addconsultationwindow.ui \
    patientselectionwindow.ui \
    physicianselectionwindow.ui \
    viewconsultationwindow.ui

OTHER_FILES += \
    images/overdue.png \
    images/stethoscope.png \
    images/patient_treatment.png \
    images/patient_chart.png \
    images/medical_record.png \
    images/ecg_chart.png \
    images/doctor.png

RESOURCES += \
    imageResources.qrc
