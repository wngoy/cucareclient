#include "patientselectionwindow.h"
#include "ui_patientselectionwindow.h"
#include "addconsultationwindow.h"
#include "ccpatientrecordcontrol.h"

#include <QMessageBox>

PatientSelectionWindow::PatientSelectionWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PatientSelectionWindow)
{
    ui->setupUi(this);

    ui->patientTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->patientTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->patientTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
}

PatientSelectionWindow::~PatientSelectionWindow()
{
    delete ui;
}


void PatientSelectionWindow::on_selectPatientButton_clicked()
{
    // Patient selected

    // Get parent to refresh
    AddConsultationWindow* acw = qobject_cast<AddConsultationWindow*>(this->parent());
    if (acw != NULL)
    {
        CCPatientRecord* selectedPatient = NULL;

        // Determine selected patient

        if (selectedPatient != NULL)
        {
            // Update with selected patient
            acw->updatePatient(selectedPatient);

            // Close window
            this->close();
        }
        else
        {
            // Message with "selected a row"
            QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No patient was selected", "You must select a patient.", QMessageBox::Ok, this);
            alert->setWindowFlags(Qt::WindowStaysOnTopHint);
            alert->show();
        }
    }
}
