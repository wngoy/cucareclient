#include "ccconsultationrecord.h"

CCConsultationRecord::CCConsultationRecord(QString aPatientName, QString aPhysicianName, CCConsultationRecordStatus aStatus,
                                           QString aClinic, QDateTime aDate, int aConsultationTypeId, QString aReasonForConsult,
                                           QString aAdditionalInfo, QString aPhysicianNotes, QString aDiagnosis) :
    patientName(aPatientName),
    physicianName(aPhysicianName),
    status(aStatus),
    clinicName(aClinic),
    dateOfConsult(aDate),
    consultationTypeId(aConsultationTypeId),
    reasonForConsultation(aReasonForConsult),
    additionalInformation(aAdditionalInfo),
    physicianNotes(aPhysicianNotes),
    diagnosis(aDiagnosis)
{

}

CCConsultationRecord::CCConsultationRecord(QString constructorString)
{

}

CCConsultationRecord::~CCConsultationRecord()
{

}
