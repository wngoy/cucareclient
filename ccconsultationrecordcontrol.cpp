#include "ccconsultationrecordcontrol.h"

CCConsultationRecordControl* CCConsultationRecordControlSingleton = 0;

CCConsultationRecordControl::CCConsultationRecordControl()
{
}

CCConsultationRecordControl::~CCConsultationRecordControl()
{

}

CCConsultationRecordControl* CCConsultationRecordControl::sharedConsultationRecordControl()
{
    if (CCConsultationRecordControlSingleton == 0)
    {
        CCConsultationRecordControlSingleton = new CCConsultationRecordControl();
    }

    return CCConsultationRecordControlSingleton;
}


CCConsultationRecord* CCConsultationRecordControl::getConsultationRecordForId(int aConsultationRecordId)
{
    if (_consultationRecordsMap.size() < 0 || (!_consultationRecordsMap.contains(aConsultationRecordId)))
    {
        this->refreshConsultationRecords();
    }

    if (_consultationRecordsMap.contains(aConsultationRecordId))
    {
        return _consultationRecordsMap[aConsultationRecordId];
    }

    return 0;
}

void CCConsultationRecordControl::refreshConsultationRecords()
{
    _consultationRecordsMap.clear();

    // call server and refresh map
}

QMap<int, CCConsultationRecord*> CCConsultationRecordControl::getConsultationRecordMap(){
    return _consultationRecordsMap;
}
