#ifndef CCCONSULTATIONTYPECONTROL_H
#define CCCONSULTATIONTYPECONTROL_H

#include "ccconsultationtype.h"
#include <QString>
#include <QMap>

class CCConsultationTypeControl
{
public:
    CCConsultationTypeControl();
    ~CCConsultationTypeControl();
    static CCConsultationTypeControl* sharedConsultationTypeControl();

    void refreshConsultationTypes();
    QString getConsultationTypeNameForId(int aConsultationTypeId);
    QMap<int, CCConsultationType*> getConsultationTypesMap();

private:
    QMap<int, CCConsultationType*> _consultationTypesMap;
};

#endif // CCCONSULTATIONTYPECONTROL_H
