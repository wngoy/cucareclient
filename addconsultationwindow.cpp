#include "addconsultationwindow.h"
#include "ui_addconsultationwindow.h"
#include "manageconsultationswindow.h"
#include "patientselectionwindow.h"
#include "physicianselectionwindow.h"
#include "ccphysicianrecord.h"
#include "ccpatientrecord.h"
#include "ccclinicrecordcontrol.h"
#include "ccconsultationtypecontrol.h"

#include <QMap>

AddConsultationWindow::AddConsultationWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AddConsultationWindow)
{
    patientWindow = NULL;
    physicianWindow = NULL;

    ui->setupUi(this);

    ui->dateOfConsultDateTime->setTime(QTime::currentTime());
    populateClinics();
    populateConsultationTypes();
}

AddConsultationWindow::~AddConsultationWindow()
{
    delete ui;
}

void AddConsultationWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void AddConsultationWindow::populateClinics()
{
    CCClinicRecordControl* clinRecControl = CCClinicRecordControl::sharedClinicRecordControl();

    ui->clinicComboBox->clear();
    clinicsList.clear();

    QMap<int, CCClinicRecord*> clinRecMap = clinRecControl->getClinicRecordsMap();
    clinicsList.reserve(clinRecMap.size());
    QMap<int, CCClinicRecord*>::iterator i;
    for (i = clinRecMap.begin(); i != clinRecMap.end(); ++i)
    {
        CCClinicRecord* record = (CCClinicRecord*)i.value();
        ui->clinicComboBox->addItem(record->clinicName);
        clinicsList.append(record);
    }
}

void AddConsultationWindow::populateConsultationTypes()
{
    CCConsultationTypeControl* typeControl = CCConsultationTypeControl::sharedConsultationTypeControl();

    ui->consultationTypeComboBox->clear();

    QMap<int, CCConsultationType*> typeMap = typeControl->getConsultationTypesMap();
    consultationsTypeList.reserve(typeMap.size());
    QMap<int, CCConsultationType*>::iterator i;
    for (i = typeMap.begin(); i != typeMap.end(); ++i)
    {
        CCConsultationType* type = (CCConsultationType*)i.value();
        ui->consultationTypeComboBox->addItem(type->typeName);
        consultationsTypeList.append(type);
    }
}

void AddConsultationWindow::on_patientSelectButton_clicked()
{
    // Pop up patient selector view
    if (patientWindow == NULL)
    {
        patientWindow = new PatientSelectionWindow(this);
        patientWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(patientWindow, SIGNAL(destroyed()), this, SLOT(clearPatientWindow()));
    }

    if (patientWindow)
    {
        Qt::WindowFlags eFlags = patientWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        patientWindow->setWindowFlags(eFlags);
        patientWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        patientWindow->setWindowFlags(eFlags);
        patientWindow->show();
    }
}

void AddConsultationWindow::clearPatientWindow()
{
    patientWindow = NULL;
}

void AddConsultationWindow::on_physicianSelectButton_clicked()
{
    // Pop up physician selector view
    if (physicianWindow == NULL)
    {
        physicianWindow = new PhysicianSelectionWindow(this);
        physicianWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(physicianWindow, SIGNAL(destroyed()), this, SLOT(clearPhysicianWindow()));
    }

    if (physicianWindow)
    {
        Qt::WindowFlags eFlags = physicianWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        physicianWindow->setWindowFlags(eFlags);
        physicianWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        physicianWindow->setWindowFlags(eFlags);
        physicianWindow->show();
    }
}

void AddConsultationWindow::clearPhysicianWindow()
{
    physicianWindow = NULL;
}

void AddConsultationWindow::on_createConsultationButton_clicked()
{
    // Save record to database

    // Get parent to refresh
    ManageConsultationsWindow* mcw = qobject_cast<ManageConsultationsWindow*>(this->parent());
    if (mcw != NULL)
    {
        mcw->refreshConsultationsTable();
    }

    // Close window
    this->close();
}

void AddConsultationWindow::updatePatient(CCPatientRecord* patient)
{
    _patientRecord = patient;

    QString patientText = "Patient: ";
    ui->patientLabel->setText(patientText.append(_patientRecord->firstName).append(" ").append(_patientRecord->lastName));
}

void AddConsultationWindow::updatePhysician(CCPhysicianRecord* physician)
{
    _physicianRecord = physician;

    QString patientText = "Physician: ";
    ui->patientLabel->setText(patientText.append(_physicianRecord->firstName).append(" ").append(_physicianRecord->lastName));
}
