#ifndef PHYSICIANSELECTIONWINDOW_H
#define PHYSICIANSELECTIONWINDOW_H

#include <QMainWindow>

namespace Ui {
class PhysicianSelectionWindow;
}

class PhysicianSelectionWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit PhysicianSelectionWindow(QWidget *parent = 0);
    ~PhysicianSelectionWindow();
    
private slots:
    void on_selectPatientButton_clicked();

private:
    Ui::PhysicianSelectionWindow *ui;
};

#endif // PHYSICIANSELECTIONWINDOW_H
