#include "ccconsultationtypecontrol.h"

CCConsultationTypeControl* CCConsultationTypeControlSingleton = 0;

CCConsultationTypeControl::CCConsultationTypeControl()
{
}

CCConsultationTypeControl::~CCConsultationTypeControl()
{
}

CCConsultationTypeControl* CCConsultationTypeControl::sharedConsultationTypeControl()
{
    if (CCConsultationTypeControlSingleton == 0)
    {
        CCConsultationTypeControlSingleton = new CCConsultationTypeControl();
    }

    return CCConsultationTypeControlSingleton;
}

QString CCConsultationTypeControl::getConsultationTypeNameForId(int aConsultationTypeId)
{
    if (_consultationTypesMap.size() < 0 || (!_consultationTypesMap.contains(aConsultationTypeId)))
    {
        this->refreshConsultationTypes();
    }

    if (_consultationTypesMap.contains(aConsultationTypeId))
    {
        return _consultationTypesMap[aConsultationTypeId]->typeName;
    }

    return "Unknown Consultation Type";
}

void CCConsultationTypeControl::refreshConsultationTypes()
{
    _consultationTypesMap.clear();

    // call server and refresh consultation types map

}

QMap<int, CCConsultationType*> CCConsultationTypeControl::getConsultationTypesMap(){
    return _consultationTypesMap;
}
