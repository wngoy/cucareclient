#include "homewindow.h"
#include "ui_homewindow.h"
#include "manageconsultationswindow.h"

HomeWindow::HomeWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HomeWindow)
{
    ui->setupUi(this);
    mcw = NULL;
}

HomeWindow::~HomeWindow()
{
    delete ui;
}

void HomeWindow::closeEvent(QCloseEvent *)
{   
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void HomeWindow::on_consultButton_clicked()
{
    if (mcw == NULL)
    {
        mcw = new ManageConsultationsWindow(this);
        mcw->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(mcw, SIGNAL(destroyed()), this, SLOT(clearManageConsultationsWindow()));
    }

    if (mcw)
    {
        Qt::WindowFlags eFlags = mcw->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        mcw->setWindowFlags(eFlags);
        mcw->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        mcw->setWindowFlags(eFlags);
        mcw->show();
    }
}

void HomeWindow::clearManageConsultationsWindow()
{
    mcw = NULL;
}

void HomeWindow::on_logoutButton_clicked()
{
    LoginWindow* l = new LoginWindow;
    l->ShowLoginScreen();
    this->close();
}
