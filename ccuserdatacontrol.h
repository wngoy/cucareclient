#ifndef CCUSERDATACONTROL_H
#define CCUSERDATACONTROL_H

#include <QString>

enum CCUserPriviledgeLevel
{
    CCUserPriviledgeLevelNoAccess = 0,
    CCUserPriviledgeLevelAdminAssistant = 1,
    CCUserPriviledgeLevelPhysician = 2,
    CCUserPriviledgeLevelSysAdmin = 3
};

class CCUserDataControl
{
public:
    CCUserDataControl();
    ~CCUserDataControl();
    static CCUserDataControl* sharedUserDataControl();

    bool loginWithUserName(QString aUserName);
    CCUserPriviledgeLevel getUserPriviledgeLevel();
    void logoutUser();

protected:
    void _clearUserData();
    QString _userName;
    CCUserPriviledgeLevel _priviledgeLevel;
};

#endif // CCUSERDATACONTROL_H
