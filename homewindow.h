#ifndef HOMEWINDOW_H
#define HOMEWINDOW_H

#include <QMainWindow>
#include "loginwindow.h"
#include "manageconsultationswindow.h"

namespace Ui {
class HomeWindow;
}

class HomeWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit HomeWindow(QWidget *parent = 0);
    ~HomeWindow();

    void closeEvent(QCloseEvent *);

private slots:
    void on_consultButton_clicked();
    void on_logoutButton_clicked();

    void clearManageConsultationsWindow();

private:
    Ui::HomeWindow *ui;
    ManageConsultationsWindow* mcw;
};

#endif // HOMEWINDOW_H
