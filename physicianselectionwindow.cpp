#include "physicianselectionwindow.h"
#include "ui_physicianselectionwindow.h"
#include "addconsultationwindow.h"
#include "ccphysicianrecordcontrol.h"

#include <QMessageBox>

PhysicianSelectionWindow::PhysicianSelectionWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PhysicianSelectionWindow)
{
    ui->setupUi(this);

    ui->physicianTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->physicianTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->physicianTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
}

PhysicianSelectionWindow::~PhysicianSelectionWindow()
{
    delete ui;
}

void PhysicianSelectionWindow::on_selectPatientButton_clicked()
{
    // Physician selected

    // Get parent to refresh
    AddConsultationWindow* acw = qobject_cast<AddConsultationWindow*>(this->parent());
    if (acw != NULL)
    {
        CCPhysicianRecord* selectedPhysician = NULL;

        // Determine selected physician

        if (selectedPhysician != NULL)
        {
            // Update with selected physician
            acw->updatePhysician(selectedPhysician);

            // Close window
            this->close();
        }
        else
        {
            // Message with "selected a row"
            QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No physician was selected", "You must select a physician.", QMessageBox::Ok, this);
            alert->setWindowFlags(Qt::WindowStaysOnTopHint);
            alert->show();
        }
    }
}
