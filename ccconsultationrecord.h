#ifndef CCCONSULTATIONRECORD_H
#define CCCONSULTATIONRECORD_H

#include <QDateTime>
#include <QString>
#include "ccconsultationtype.h"

enum CCConsultationRecordStatus
{
    CCConsultationRecordStatusPending = 0,
    CCConsultationRecordStatusComplete = 1,
    CCConsultationRecordStatusOverdue = 2
};

class CCConsultationRecord
{
public:
    explicit CCConsultationRecord(QString aPatientName, QString aPhysicianName, CCConsultationRecordStatus aStatus,
                                  QString aClinic, QDateTime aDate, int aConsultationTypeId, QString aReasonForConsult = "",
                                  QString aAdditionalInfo = "", QString aPhysicianNotes = "", QString aDiagnosis = "");
    explicit CCConsultationRecord(QString constructorString);
    ~CCConsultationRecord();

    CCConsultationRecordStatus status;
    QString physicianName;
    QString patientName;
    QString clinicName;
    QDateTime dateOfConsult;
    int consultationTypeId;
    QString reasonForConsultation;
    QString additionalInformation;
    QString physicianNotes;
    QString diagnosis;
};

#endif // CCCONSULTATIONRECORD_H
