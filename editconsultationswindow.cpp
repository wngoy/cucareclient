#include "editconsultationswindow.h"
#include "ui_editconsultationswindow.h"
#include "ccconsultationtypecontrol.h"
#include "manageconsultationswindow.h"

EditConsultationsWindow::EditConsultationsWindow(CCConsultationRecord *aRecord, QWidget *parent) :
    QMainWindow(parent),
    consultationRecord(aRecord),
    ui(new Ui::EditConsultationsWindow)
{
    ui->setupUi(this);

    this->_setupViewWithRecord();
}

EditConsultationsWindow::~EditConsultationsWindow()
{
    delete ui;
}

void EditConsultationsWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void EditConsultationsWindow::_setupViewWithRecord()
{
    QString startBitString = "";

    startBitString = "Patient: ";
    ui->patientLabel->setText(startBitString.append(consultationRecord->patientName));

    startBitString = "Consulting Physician: ";
    ui->physicianLabel->setText(startBitString.append(consultationRecord->physicianName));

    startBitString = "Clinic: ";
    ui->clinicLabel->setText(startBitString.append(consultationRecord->clinicName));

    startBitString = "Consultation Type: ";
    ui->consultationTypeLabel->setText(startBitString.append(CCConsultationTypeControl::sharedConsultationTypeControl()->getConsultationTypeNameForId(consultationRecord->consultationTypeId)));

    ui->statusComboBox->setCurrentIndex((int)consultationRecord->status);
    if (ui->statusComboBox->currentIndex() > 0)
    {
        ui->statusComboBox->setEnabled(false);
    }

    ui->additionalInformationTextEdit->setText(consultationRecord->additionalInformation);
    ui->physicianNotesTextEdit->setText(consultationRecord->physicianNotes);
    ui->diagnosisTextEdit->setText(consultationRecord->diagnosis);
    ui->reasonForConsultationTextEdit->setText(consultationRecord->reasonForConsultation);

    ui->dateOfConsultDateTime->setDateTime(consultationRecord->dateOfConsult);
}

void EditConsultationsWindow::on_saveChangesButton_clicked()
{
    // Save record to database

    // Get parent to refresh
    ManageConsultationsWindow* mcw = qobject_cast<ManageConsultationsWindow*>(this->parent());
    if (mcw != NULL)
    {
        mcw->refreshConsultationsTable();
    }

    // Close window
    this->close();
}
