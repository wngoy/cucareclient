#ifndef MANAGECONSULTATIONSWINDOW_H
#define MANAGECONSULTATIONSWINDOW_H

#include <QMainWindow>

#include "addconsultationwindow.h"
#include "editconsultationswindow.h"

class CCConsultationRecord;

namespace Ui {
class ManageConsultationsWindow;
}

class ManageConsultationsWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ManageConsultationsWindow(QWidget *parent = 0);
    ~ManageConsultationsWindow();
    void refreshConsultationsTable();

    void closeEvent(QCloseEvent *);

private slots:
    bool _addConsultationToTable(CCConsultationRecord* record);
    void on_actionAdd_New_Consultation_triggered();
    void on_actionEdit_Selected_Consultation_triggered();
    void clearAddWindow();
    void clearEditWindow();

private:
    Ui::ManageConsultationsWindow *ui;
    AddConsultationWindow* addWindow;
    EditConsultationsWindow* editWindow;
};

#endif // MANAGECONSULTATIONSWINDOW_H
