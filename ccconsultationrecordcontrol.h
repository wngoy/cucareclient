#ifndef CCCONSULTATIONRECORDCONTROL_H
#define CCCONSULTATIONRECORDCONTROL_H

#include "ccconsultationrecord.h"
#include <QMap>

class CCConsultationRecordControl
{
public:
    CCConsultationRecordControl();
    ~CCConsultationRecordControl();
    static CCConsultationRecordControl* sharedConsultationRecordControl();

    void refreshConsultationRecords();
    CCConsultationRecord* getConsultationRecordForId(int aConsultationRecordId);

    QMap<int, CCConsultationRecord*> getConsultationRecordMap();

private:
    QMap<int, CCConsultationRecord*> _consultationRecordsMap;
};

#endif // CCCONSULTATIONRECORDCONTROL_H
