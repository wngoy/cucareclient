#include "ccuserdatacontrol.h"
#include <QString>

CCUserDataControl* CCUserDataControlSingleton = 0;

CCUserDataControl::CCUserDataControl() :
    _userName(""),
    _priviledgeLevel(CCUserPriviledgeLevelNoAccess)
{
}

CCUserDataControl::~CCUserDataControl()
{
}

CCUserDataControl* CCUserDataControl::sharedUserDataControl()
{
    if (CCUserDataControlSingleton == 0)
    {
        CCUserDataControlSingleton = new CCUserDataControl();
    }

    return CCUserDataControlSingleton;
}

bool CCUserDataControl::loginWithUserName(QString aUserName)
{
    _userName = aUserName;

    bool loginSuccessful = false;

    // Attempt login

    if (loginSuccessful)
    {
        // Get priviledge level

    }
    else
    {
        // Clear user data
        this->_clearUserData();
    }

    return loginSuccessful;
}

CCUserPriviledgeLevel CCUserDataControl::getUserPriviledgeLevel()
{
    return _priviledgeLevel;
}

void CCUserDataControl::logoutUser()
{
    if (_userName != "")
    {
        // Log out user from server

        // Clear user information
        this->_clearUserData();
    }
}

void CCUserDataControl::_clearUserData()
{
    _userName = "";
    _priviledgeLevel = CCUserPriviledgeLevelNoAccess;
}
