#include "ccclinicrecordcontrol.h"

CCClinicRecordControl* CCClinicRecordControlSingleton = 0;

CCClinicRecordControl::CCClinicRecordControl()
{
}

CCClinicRecordControl::~CCClinicRecordControl()
{
}

CCClinicRecordControl* CCClinicRecordControl::sharedClinicRecordControl()
{
    if (CCClinicRecordControlSingleton == 0)
    {
        CCClinicRecordControlSingleton = new CCClinicRecordControl();
    }

    return CCClinicRecordControlSingleton;
}

QString CCClinicRecordControl::getClinicNameForId(int aClinicId)
{
    if (_clinicRecordsMap.size() < 0 || (!_clinicRecordsMap.contains(aClinicId)))
    {
        this->refreshClinicRecords();
    }

    if (_clinicRecordsMap.contains(aClinicId))
    {
        return _clinicRecordsMap[aClinicId]->clinicName;
    }

    return "Unknown Clinic";
}

void CCClinicRecordControl::refreshClinicRecords()
{
    _clinicRecordsMap.clear();

    // call server and refresh clinics map

}

QMap<int, CCClinicRecord*> CCClinicRecordControl::getClinicRecordsMap()
{
    return _clinicRecordsMap;
}
