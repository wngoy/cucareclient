#ifndef CCPHYSICIANRECORDCONTROL_H
#define CCPHYSICIANRECORDCONTROL_H

#include <QMap>
#include "ccphysicianrecord.h"

class CCPhysicianRecordControl
{
public:
    CCPhysicianRecordControl();
    ~CCPhysicianRecordControl();
    static CCPhysicianRecordControl* sharedPhysicianRecordControl();

    void refreshPhysicianRecords();
    CCPhysicianRecord* getPhysicianRecordForId(int aPhysicianRecordId);
    QMap<int, CCPhysicianRecord*> getPhysicianRecordsMap();

private:
    QMap<int, CCPhysicianRecord*> _physicianRecordsMap;

};

#endif // CCPHYSICIANRECORDCONTROL_H
