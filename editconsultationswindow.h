#ifndef EDITCONSULTATIONSWINDOW_H
#define EDITCONSULTATIONSWINDOW_H

#include <QMainWindow>
#include "ccconsultationrecord.h"

namespace Ui {
class EditConsultationsWindow;
}

class EditConsultationsWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit EditConsultationsWindow(CCConsultationRecord* aRecord, QWidget *parent = 0);
    ~EditConsultationsWindow();
    void closeEvent(QCloseEvent *);

    CCConsultationRecord* consultationRecord;

private slots:
    void on_saveChangesButton_clicked();

private:
    Ui::EditConsultationsWindow *ui;

    void _setupViewWithRecord();
};

#endif // EDITCONSULTATIONSWINDOW_H
