#ifndef PATIENTSELECTIONWINDOW_H
#define PATIENTSELECTIONWINDOW_H

#include <QMainWindow>

namespace Ui {
class PatientSelectionWindow;
}

class PatientSelectionWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit PatientSelectionWindow(QWidget *parent = 0);
    ~PatientSelectionWindow();
    
private slots:
    void on_selectPatientButton_clicked();

private:
    Ui::PatientSelectionWindow *ui;
};

#endif // PATIENTSELECTIONWINDOW_H
