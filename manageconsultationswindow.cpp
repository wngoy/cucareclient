#include "manageconsultationswindow.h"
#include "ui_manageconsultationswindow.h"
#include "ccconsultationrecord.h"
#include "ccutils.h"
#include "editconsultationswindow.h"
#include "addconsultationwindow.h"

#include <QMessageBox>
#include <QModelIndexList>
#include <QObjectList>

ManageConsultationsWindow::ManageConsultationsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ManageConsultationsWindow)
{
    addWindow = NULL;
    editWindow = NULL;

    ui->setupUi(this);
    ui->consultationTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->consultationTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->consultationTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    //**** TODO: Replace with fetch code that populates table and an array with created consultations ****//
    //on_actionAdd_New_Consultation_triggered();
    //on_actionAdd_New_Consultation_triggered();
}

ManageConsultationsWindow::~ManageConsultationsWindow()
{
    delete ui;
}

bool ManageConsultationsWindow::_addConsultationToTable(CCConsultationRecord* record)
{
    int rowIndex = ui->consultationTableWidget->rowCount();
    ui->consultationTableWidget->insertRow(rowIndex);

    QString statusString = CCUtils::getStatusStringForStatus(record->status);

    ui->consultationTableWidget->setItem(rowIndex, 0, new QTableWidgetItem(statusString));
    ui->consultationTableWidget->setItem(rowIndex, 1, new QTableWidgetItem(record->patientName));
    ui->consultationTableWidget->setItem(rowIndex, 2, new QTableWidgetItem(record->physicianName));
    ui->consultationTableWidget->setItem(rowIndex, 3, new QTableWidgetItem(record->clinicName));
    ui->consultationTableWidget->setItem(rowIndex, 4, new QTableWidgetItem(record->dateOfConsult.toString()));

    ui->consultationTableWidget->setVerticalHeaderItem(rowIndex, new QTableWidgetItem(""));

    return true;
}

void ManageConsultationsWindow::on_actionAdd_New_Consultation_triggered()
{
    // Open new add window
    if (addWindow == NULL)
    {
        addWindow = new AddConsultationWindow(this);
        addWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(addWindow, SIGNAL(destroyed()), this, SLOT(clearAddWindow()));
    }

    if (addWindow)
    {
        Qt::WindowFlags eFlags = addWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        addWindow->setWindowFlags(eFlags);
        addWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        addWindow->setWindowFlags(eFlags);
        addWindow->show();
    }

    // Fake add for now
    //**** TODO: Change to widget creator ****//
    /*
    srand(time(0));

    CCConsultationRecord* record = new CCConsultationRecord("Fake Patient", "Dr. A. Nonymous", (CCConsultationRecordStatus)(rand() % 3), "Clinic of the Lost Ark", QDateTime::currentDateTime(), -1);
    this->_addConsultationToTable(record);
    */
}

void ManageConsultationsWindow::clearAddWindow()
{
    addWindow = NULL;
}

void ManageConsultationsWindow::on_actionEdit_Selected_Consultation_triggered()
{
    QModelIndexList selectedRows = ui->consultationTableWidget->selectionModel()->selectedRows();
    if (selectedRows.count() > 0)
    {
        int index = selectedRows.at(0).row();

        // Check that the index exists within the array and grab the consultations record
        /*
        if (index >= 0 && index < <ARRAY>.count())
        {
        CCConsultationRecord* record = <ARRAY>.at(index);

        if (editWindow == NULL)
        {
            editWindow = new AddConsultationWindow(this);
            editWindow->setAttribute(Qt::WA_DeleteOnClose, true);
            QObject::connect(editWindow, SIGNAL(destroyed()), this, SLOT(clearEditWindow()));
        }

        if (addWindow)
        {
            Qt::WindowFlags eFlags = editWindow->windowFlags();
            eFlags |= Qt::WindowStaysOnTopHint;
            editWindow->setWindowFlags(eFlags);
            editWindow->show();
            eFlags &= ~Qt::WindowStaysOnTopHint;
            editWindow->setWindowFlags(eFlags);
            editWindow->show();
        }
        }
        */
    }
    else
    {
        // Message with "selected a row to edit"
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No consultation was selected", "You must select a consultation in order to edit.", QMessageBox::Ok, this);
        alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }
}

void ManageConsultationsWindow::clearEditWindow()
{
    editWindow = NULL;
}

void ManageConsultationsWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void ManageConsultationsWindow::refreshConsultationsTable()
{
    // Refresh yo!
}
