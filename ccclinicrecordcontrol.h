#ifndef CCCLINICRECORDCONTROL_H
#define CCCLINICRECORDCONTROL_H

#include "ccclinicrecord.h"
#include <QMap>
#include <QString>

class CCClinicRecordControl
{
public:
    CCClinicRecordControl();
    ~CCClinicRecordControl();
    static CCClinicRecordControl* sharedClinicRecordControl();

    void refreshClinicRecords();
    QString getClinicNameForId(int aClinicId);

    QMap<int, CCClinicRecord*> getClinicRecordsMap();

private:
    QMap<int, CCClinicRecord*> _clinicRecordsMap;
};

#endif // CCCLINICRECORDCONTROL_H
