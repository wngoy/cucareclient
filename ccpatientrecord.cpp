#include "ccpatientrecord.h"

CCPatientRecord::CCPatientRecord(QString aFirstName, QString aLastName, int aOhipNumber,
                             QDate aDateOfBirth, int aAreaCode, int aStationCode, int aDirectoryCode,
                             QString aAddress1, QString aAddress2,
                             QString aProvince, QString aCountry, QString aPostalCode) :
    firstName(aFirstName),
    lastName(aLastName),
    ohipNumber(aOhipNumber),
    dateOfBirth(aDateOfBirth),
    areaCode(aAreaCode),
    stationCode(aStationCode),
    directoryCode(aDirectoryCode),
    address1(aAddress1),
    address2(aAddress2),
    province(aProvince),
    country(aCountry),
    postalCode(aPostalCode)
{
}

CCPatientRecord::CCPatientRecord(QString constructorString)
{

}

CCPatientRecord::~CCPatientRecord()
{

}
