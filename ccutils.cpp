#include "ccutils.h"

QString CCUtils::getStatusStringForStatus(CCConsultationRecordStatus status)
{
    switch(status)
    {
        case CCConsultationRecordStatusOverdue:
            return "Overdue";
            break;
        case CCConsultationRecordStatusPending:
            return "Pending";
            break;
        case CCConsultationRecordStatusComplete:
            return "Complete";
            break;
        default:
            return "Unknown";
            break;
    }
}
