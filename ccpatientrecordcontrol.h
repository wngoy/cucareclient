#ifndef CCPATIENTRECORDCONTROL_H
#define CCPATIENTRECORDCONTROL_H

#include "ccpatientrecord.h"
#include <QMap>

class CCPatientRecordControl
{
public:
    CCPatientRecordControl();
    ~CCPatientRecordControl();
    static CCPatientRecordControl* sharedPatientRecordControl();

    void refreshPatientRecords();
    CCPatientRecord* getPatientRecordForId(int aPatientRecordId);
    QMap<int, CCPatientRecord*> getPatientRecordsMap();

private:
    QMap<int, CCPatientRecord*> _patientRecordsMap;
};

#endif // CCPATIENTRECORDCONTROL_H
