#include "ccphysicianrecordcontrol.h"

CCPhysicianRecordControl::CCPhysicianRecordControl()
{
}

CCPhysicianRecordControl::~CCPhysicianRecordControl()
{

}

CCPhysicianRecord* CCPhysicianRecordControl::getPhysicianRecordForId(int aPhysicianRecordId)
{
    if (_physicianRecordsMap.size() < 0 || (!_physicianRecordsMap.contains(aPhysicianRecordId)))
    {
        this->refreshPhysicianRecords();
    }

    if (_physicianRecordsMap.contains(aPhysicianRecordId))
    {
        return _physicianRecordsMap[aPhysicianRecordId];
    }

    return 0;
}

void CCPhysicianRecordControl::refreshPhysicianRecords()
{
    _physicianRecordsMap.clear();

    // call server and refresh map
}

QMap<int, CCPhysicianRecord*> CCPhysicianRecordControl::getPhysicianRecordsMap(){
    return _physicianRecordsMap;
}
